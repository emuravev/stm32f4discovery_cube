/*
*  BT ������ ��05 ������� �� ����� GW040
*  ��� �������� � "����" �� ����� ���� ���������� ������
*  ������ �������� �������� �� ������� 38400
*  ������������ �� ����� �������� ��� ��������
*  �� ����� - ������ ������� �����������.
*
*/

/* Includes ------------------------------------------------------------------*/
#include "hc05.h"
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*	Local variables	*/
uint32_t bt_speed[] = {9600,19200,38400,57600,115200,230400,460800};
unsigned char usartRXBuf[50];
unsigned char *rx_point = &usartRXBuf[0];
extern UART_HandleTypeDef huart2;

void hc05WriteComand ( char* buffer, unsigned int size)
{
  	unsigned char usartTXBuf[20];
  	memcpy (usartTXBuf, buffer, size);
	HAL_UART_Transmit  (&huart2, usartTXBuf ,size,150);
	//HAL_UART_Receive   (&huart2, usartRXBuf ,sizeof(usartRXBuf), 100);
}

/*
	������������ ������. �������� ���� ��� ������� ������� ��������.
	� ������ �� ������ ������ �������� �� ������� 3840
*/
void hc05Config (void)
{
  // ��� ������������� ������ ������ � ������ �������.
  // �������� �������� ����� 38400
  		huart2.Init.BaudRate = 38400;
  		HAL_UART_Init(&huart2);
		
   		hc05WriteComand ("AT\r\n",4);
		HAL_UART_Receive (&huart2, usartRXBuf,4,4*10+10);
		
		hc05WriteComand ("at+name=sss\r\n",13);
		HAL_UART_Receive (&huart2, usartRXBuf,4,4*10+10);
  		
		hc05WriteComand ("at+name?\r\n",11);
  		HAL_UART_Receive (&huart2, usartRXBuf,10,10*10+100);
		
		hc05WriteComand ("AT\r\n",4);
  //����� ��� �� board
  ///hc05WriteComand ("AT+NAME=BOrRD\r\n",15);
  }


// ������� ���������� ���� ��� ��� ������������ ������.
void hc05Init (void)
{
 // ��������� ��� AT ������ 
 huart2.Init.BaudRate = 38400;
 huart2.Init.WordLength = UART_WORDLENGTH_8B;
 huart2.Init.StopBits = UART_STOPBITS_1;
 huart2.Init.Mode = UART_MODE_TX_RX;
 HAL_UART_Init(&huart2);
 
 // ��������� �������� 460800
HAL_Delay(300);
 hc05WriteComand ("AT+NAME=BOARD\r\n",15);
 HAL_Delay(300);
 hc05WriteComand ("AT+UART=460800,1,0\r\n",20);
 HAL_Delay(300);
 hc05WriteComand ("AT+RESET\r\n",10); // ����� � �����
 
 huart2.Init.BaudRate = 460800;
 HAL_UART_Init(&huart2);
 HAL_Delay(3000);
 return;
}

