#include "stm32f4xx_hal.h"

#include "mpu_6050.h"


unsigned char MPU_addr = 0x68<<1; // адрес гироскопа на шине i2c
extern I2C_HandleTypeDef hi2c1;

unsigned char reg_read (uint8_t reg)
{
  uint8_t data;
 
  HAL_I2C_Master_Transmit(&hi2c1, MPU_addr, &reg, sizeof(reg), 30);
  HAL_I2C_Master_Receive(&hi2c1, MPU_addr, &data,  sizeof(data), 30);
  return data;
};

void reg_write (uint8_t reg, uint8_t data)
{
  HAL_I2C_Master_Transmit(&hi2c1, MPU_addr, &reg, sizeof(reg), 30);
  HAL_I2C_Master_Transmit(&hi2c1, MPU_addr, &data,  sizeof(data), 30);
};


int who_am_i_mpu6050(void) 
{
	if (reg_read(MPU6050_RA_WHO_AM_I)==(MPU_addr>>1)) return 1; 
      	return 0;
};

// инициализация, конфигурация
void mpu6050_init (void) {

	//Sets sample rate to 8000/1+7 = 1000Hz
	reg_write (MPU6050_RA_SMPLRT_DIV, 0x07);
	
	//Disable FSync, 256Hz DLPF
	reg_write (MPU6050_RA_CONFIG, 0x00);

	//Disable gyro self tests, scale of 2000 degrees/s
	reg_write (MPU6050_RA_GYRO_CONFIG, 0x18);

	//Disable accel self tests, scale of +-2g, no DHPF
	reg_write (MPU6050_RA_ACCEL_CONFIG, 0x00);

	//Freefall threshold of |0mg|
	reg_write (MPU6050_RA_FF_THR, 0x00);

	//Freefall duration limit of 0
	reg_write (MPU6050_RA_FF_DUR, 0x00);

	//Motion threshold of 0mg
	reg_write (MPU6050_RA_MOT_THR, 0x00);

	//Motion duration of 0s
	reg_write (MPU6050_RA_MOT_DUR, 0x00);

	//Zero motion threshold
	reg_write (MPU6050_RA_ZRMOT_THR, 0x00);

	//Zero motion duration threshold
	reg_write (MPU6050_RA_ZRMOT_DUR, 0x00);

	//Disable sensor output to FIFO buffer
	reg_write (MPU6050_RA_FIFO_EN, 0x00);



	//AUX I2C setup
	//Sets AUX I2C to single master control, plus other config
	reg_write (MPU6050_RA_I2C_MST_CTRL, 0x00);
	//Setup AUX I2C slaves
	reg_write (MPU6050_RA_I2C_SLV0_ADDR, 0x00);
	reg_write (MPU6050_RA_I2C_SLV0_REG, 0x00);
	reg_write (MPU6050_RA_I2C_SLV0_CTRL, 0x00);
	reg_write (MPU6050_RA_I2C_SLV1_ADDR, 0x00);
	reg_write (MPU6050_RA_I2C_SLV1_REG, 0x00);
	reg_write (MPU6050_RA_I2C_SLV1_CTRL, 0x00);
	reg_write (MPU6050_RA_I2C_SLV2_ADDR, 0x00);
	reg_write (MPU6050_RA_I2C_SLV2_REG, 0x00);
	reg_write (MPU6050_RA_I2C_SLV2_CTRL, 0x00);
	reg_write (MPU6050_RA_I2C_SLV3_ADDR, 0x00);
	reg_write (MPU6050_RA_I2C_SLV3_REG, 0x00);
	reg_write (MPU6050_RA_I2C_SLV3_CTRL, 0x00);
	reg_write (MPU6050_RA_I2C_SLV4_ADDR, 0x00);
	reg_write (MPU6050_RA_I2C_SLV4_REG, 0x00);
	reg_write (MPU6050_RA_I2C_SLV4_DO, 0x00);
	reg_write (MPU6050_RA_I2C_SLV4_CTRL, 0x00);
	reg_write (MPU6050_RA_I2C_SLV4_DI, 0x00);
	//MPU6050_RA_I2C_MST_STATUS //Read-only
	//Setup INT pin and AUX I2C pass through
	reg_write (MPU6050_RA_INT_PIN_CFG, 0x00);
	//Enable data ready interrupt
	reg_write (MPU6050_RA_INT_ENABLE, 0x00);


	//Slave out, dont care
	reg_write (MPU6050_RA_I2C_SLV0_DO, 0x00);
	reg_write (MPU6050_RA_I2C_SLV1_DO, 0x00);
	reg_write (MPU6050_RA_I2C_SLV2_DO, 0x00);
	reg_write (MPU6050_RA_I2C_SLV3_DO, 0x00);
	//More slave config
	reg_write (MPU6050_RA_I2C_MST_DELAY_CTRL, 0x00);
	//Reset sensor signal paths
	reg_write (MPU6050_RA_SIGNAL_PATH_RESET, 0x00);
	//Motion detection control
	reg_write (MPU6050_RA_MOT_DETECT_CTRL, 0x00);
	//Disables FIFO, AUX I2C, FIFO and I2C reset bits to 0
	reg_write (MPU6050_RA_USER_CTRL, 0x00);
	//Sets clock source to gyro reference w/ PLL
	reg_write (MPU6050_RA_PWR_MGMT_1, 2);
	//Controls frequency of wakeups in accel low power mode plus the sensor standby modes
	reg_write (MPU6050_RA_PWR_MGMT_2, 0x00);
};



// температура
uint16_t get_temp (void) {
  
	return reg_read (MPU6050_RA_TEMP_OUT_H)*256 + reg_read (MPU6050_RA_TEMP_OUT_L);
}